import React from 'react';

import styled, {css} from 'styled-components';
import PropTypes from 'prop-types';

const setFont = ({variant, fontSize}) => {
  return css`
      ${variant !== 'normal' ?
      `font-size: ${fontSize * 2}px` : `font-size: ${fontSize}px`}
   `;
}

export const Button = styled.button`
  width: 100px;
  background: ${p => p.variant === 'warning' ? 'red' : 'blue'};
  ${setFont};
`;

Button.propTypes = {
  variant: PropTypes.oneOf(['normal', 'warning']),
  fontSize: PropTypes.number
}

Button.defaultProps = {
  variant: 'normal',
  fontSize: 12
}
