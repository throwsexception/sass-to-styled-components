import React, { Component } from 'react';
import {Button} from './button';
import {MyButton} from './newButton'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div>
          <Button>Hello World</Button>
          <Button variant="warning">Hello World</Button>
        </div>
        <div>
          <MyButton>Hello World</MyButton>
          <MyButton variant="warning">Hello World</MyButton>
        </div>
      </div>
    );
  }
}

export default App;
