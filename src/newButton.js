import React from 'react';

import styled, {css} from 'styled-components';
import PropTypes from 'prop-types';

//import styles from '../sass/button.scss';
import styles from '!!sass-variable-loader!./../sass/button.scss';

console.log(styles);


const setFont = ({variant, fontSize}) => {
  return css`
      ${variant !== 'normal' ?
      `font-size: ${fontSize * 2}px` : `font-size: ${fontSize}px`}
   `;
}

export const MyButton = styled.button`
  width: ${styles.width};
  background: ${p => p.variant === 'warning' ? styles.red : styles.blue};
  ${setFont};
`;

MyButton.propTypes = {
  variant: PropTypes.oneOf(['normal', 'warning']),
  fontSize: PropTypes.oneOf(styles.fontSizes.split(' ').map(item => `${item}`))
}

MyButton.defaultProps = {
  variant: 'normal',
  fontSize: styles.fontSizes.split(' ').map(item => `${item}`)[0]
}
