var sass = require('node-sass');
var path = require('path');

sass.render({
  file: path.resolve(__dirname, './sass/button.scss'),
}, (err, result) => {
  console.log(result.css.toString())
});
